package com.hextaine.hexutils;
/*
 * Created by Hextaine on 11/24/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

@FunctionalInterface
public interface Hexecutor<T> {
    public abstract void execute(T t);
}
