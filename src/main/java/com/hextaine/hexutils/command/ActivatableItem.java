package com.hextaine.hexutils.command;
/*
 * Created by Hextaine on 11/24/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.Hexecutor;
import com.hextaine.hexutils.listener.HexListener;
import com.hextaine.hexutils.listener.ItemInteractionListener;
import org.bukkit.command.Command;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ActivatableItem {
    private ItemStack item;
    private Command cmd;
    private HexListener listener;
    private Hexecutor<PlayerInteractEvent> hexec;
    
    public ActivatableItem(ItemStack item, Hexecutor<PlayerInteractEvent> hexec) {
        this.item = item;
        this.hexec = hexec;
    }
    
    public boolean enable() {
        if (listener == null) {
            listener = new ItemInteractionListener(item, hexec);
            return listener.enable();
        }
        return false;
    }
    
    public boolean disable() {
        return this.listener.disable();
    }
}
