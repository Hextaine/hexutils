package com.hextaine.hexutils.area;
/*
 * Created by Hextaine on 8/16/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.Location;

import java.util.HashSet;

public class PolyRectangularRegion extends PolyRegion {
    private HashSet<RectangularRegion> regions;
    
    public PolyRectangularRegion() {
        regions = new HashSet<>();
    }

    public PolyRectangularRegion(HashSet<RectangularRegion> regions) {
        this.regions = regions;
    }

    /**
     * Adds another rectangular region onto the 'polyregion'.
     *
     * @param bound1 one of the bounds of a rectangular region
     * @param bound2 one of the bounds of a rectangular region
     */
    public void expand(Location bound1, Location bound2) {
        RectangularRegion addOnRegion = new RectangularRegion(bound1, bound2);
        expand(addOnRegion);
    }

    /**
     * Adds the com.hextaine.hexutils.area of a rectangular region to the polyregion.
     *
     * @param addOnRegion the region to add to the polyregion
     */
    public void expand(RectangularRegion addOnRegion) {
        for (RectangularRegion existingRegion : regions) {
            // Check if the addOnRegion is already inside another region
            if (existingRegion.getArea().containsAll(addOnRegion.getArea()))
                return;

            // Check if the new region encompasses an old region and remove that old region if so
            if (addOnRegion.getArea().containsAll(existingRegion.getArea())) {
                regions.remove(existingRegion);
                regions.add(addOnRegion);
                return;
            }
        }

        regions.add(addOnRegion);
    }
    
    /**
     * Removes a provided region from the polyregion.
     *
     * @param removeRegion the rectangular region to remove from the polyregion
     */
    public void contract(RectangularRegion removeRegion) {
        for (RectangularRegion existing : regions) {
            if (removeRegion.getArea().containsAll(existing.getArea())) {
                regions.remove(existing);
            } else {
                existing.removeIntersecting(removeRegion);
            }
        }
    }
}
