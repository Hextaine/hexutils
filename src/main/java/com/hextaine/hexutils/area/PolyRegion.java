package com.hextaine.hexutils.area;
/*
 * Created by Hextaine on 8/17/2019.
 * All questions can be directed to Hextaine@gmail.com
 */

import java.util.Arrays;
import java.util.HashSet;

public class PolyRegion extends Region {
    private HashSet<Region> regions;
    
    /**
     * The base constructor which initializes everything to default
     */
    public PolyRegion() {
        regions = new HashSet<>();
    }
    
    /**
     * Initializes the PolyRegion to contain a single region.
     *
     * @param r the starting region
     */
    public PolyRegion(Region r) {
        this();
        regions.add(r);
    }
    
    /**
     * Initializes the PolyRegion to contain many regions.
     *
     * @param regions the regions (in any multitude of forms) all of which will
     *                be encompassed by the PolyRegion
     */
    public PolyRegion(Region... regions) {
        this();
        this.regions.addAll(Arrays.asList(regions));
    }
    
    /**
     * Duplicates the region
     *
     * @return a shallow copy of the polyregions
     */
    public Region copy() {
        PolyRegion copy = new PolyRegion();
        for (Region r : this.regions) {
            copy.regions.add(r.copy());
        }
        
        return copy;
    }
}
