package com.hextaine.hexutils.area;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.location.HexLocation;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public abstract class Region {
    protected HashSet<Block> area;

    public Collection<Block> getArea() {
        return area;
    }

    /**
     * Shifts each block within the com.hextaine.hexutils.area to new coordinates relative to the input.
     * Each input is a double to allow for overriding with more specific shift
     * increments.
     *
     * @param shiftX the amount of distance to shift along the X axis
     * @param shiftY the amount of distance to shift along the Y axis
     * @param shiftZ the amount of distance to shift along the Z axis
     */
    public void shift(double shiftX, double shiftY, double shiftZ) {
        ArrayList<Block> newArea = new ArrayList<>();

        for (Block b : area) {
            area.remove(b);
            area.add(b.getRelative((int) shiftX, (int) shiftY, (int) shiftZ));
        }
    }
    
    /**
     * Creates a copy from the instance where all the blocks are shallow copied since
     * blocks exist statically in the game.
     *
     * @return a shallow copy of the region
     */
    public abstract Region copy();
    
    /**
     * Converts the current region into a string which contains all information
     * necessary to rebuild the same region when parsed. It is not meant to be
     * easily human readable.
     *
     * @return the storage string for the current region
     */
    @Override
    public String toString() {
        StringBuilder storageString = new StringBuilder();
        
        for (Block b : area) {
            storageString.append(new HexLocation(b.getLocation()).toSimpleString());
        }
        
        return storageString.toString();
    }
    
    /**
     * Materializes a region from a storage string.
     *
     * @param raw the raw storage string containing information to build a region
     */
    public void getAreaFromString(String raw) {
        String[] semiColonSeparated = raw.split(";");
        for (String rawLocation : semiColonSeparated) {
            this.area.add(HexLocation.fromSimpleString(rawLocation).getBlock());
        }
    }
    
    /**
     * Removes the intersecting com.hextaine.hexutils.area from between the regions. This is a convenience
     * method for removing a collection of blocks.
     *
     * @param region the region whose com.hextaine.hexutils.area will be remove from the current region's
     */
    public void removeIntersecting(Region region) {
        this.removeIntersection(region.getArea());
    }
    
    /**
     * Removes the intersecting com.hextaine.hexutils.area from the current region's com.hextaine.hexutils.area.
     *
     * @param area the collection of blocks to remove from the current com.hextaine.hexutils.area's
     *             collection
     */
    public void removeIntersection(Collection<Block> area) {
        this.area.removeAll(area);
    }
}
