package com.hextaine.hexutils.area;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.location.HexLocation;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.Collection;
import java.util.HashSet;

public class RectangularRegion extends Region implements Expandable {
    private HexLocation lower;
    private HexLocation upper;
    
    /**
     * Initializes a rectangular region by establishing the corners of the region.
     * The com.hextaine.hexutils.area of the region isn't calculated / initialized until it's checked.
     *
     * @param loc1 one of the corners of the region
     * @param loc2 one of the corners of the region
     */
    public RectangularRegion(Location loc1, Location loc2) {
        // Convert each com.hextaine.hexutils.location to HexLocations for convenience functions
        HexLocation first = new HexLocation(loc1);
        HexLocation second = new HexLocation(loc2);

        // Using the convenience functions, calculate the lowest corner and highest corner
        lower = first.combineLowest(second);
        upper = first.combineHigher(second);
    }
    
    @Override
    public Collection<Block> getArea() {
        if (area.isEmpty()) {
            calculateArea();
        }
        
        return area;
    }
    
    /**
     * Iterates over the com.hextaine.hexutils.area of the region by using relative block locations.
     * This method is costly. It adds each block individually to the com.hextaine.hexutils.area which
     * means it takes X*Y*Z time.
     */
    public void calculateArea() {
        int offsetX = 0;
        int offsetY = 0;
        int offsetZ = 0;
    
        this.area = RectangularRegion.calculateArea(lower, upper);
    }
    
    public static HashSet<Block> calculateArea(Location loc1, Location loc2) {
        int offsetX = 0;
        int offsetY = 0;
        int offsetZ = 0;
        
        HexLocation lower = new HexLocation(loc1).combineLowest(loc2);
        HexLocation upper = new HexLocation(loc1).combineHigher(loc2);
    
        HashSet<Block> area = new HashSet<>();
        
        Block lowerBlock = lower.getWorld().getBlockAt(lower);
    
        // Iteration is inclusive on both ends (expains the <=)
        while (lower.getBlockY() + offsetY <= upper.getBlockY()) {
            while (lower.getBlockZ() + offsetZ <= upper.getBlockZ()) {
                while (lower.getBlockX() + offsetX <= upper.getBlockX()) {
                    area.add(lowerBlock.getRelative(offsetX, offsetY, offsetZ));
                    offsetX++;
                }
                offsetX = 0;
            
                offsetZ++;
            }
            offsetZ = 0;
        
            offsetY++;
        }
        // offsetY = 0;
        
        return area;
    }
    
    /**
     * Increases the size of the region by expanding an endpoint of the
     * rectangular region relative to a direction by an amount.
     *
     * @param direction the direction in which to expand the region, i.e., North,
     *                  South, West, East, Up, Down
     * @param amount the number of blocks to move the corner by
     * @return the region which was expanded (this region)
     */
    public RectangularRegion expand(BlockFace direction, int amount) {
        HexLocation newUpper = null;
        HexLocation newLower = null;
        
        if (direction.getModX() + direction.getModY() + direction.getModZ() > 0)
          newUpper = new HexLocation(upper.add(direction.getDirection().multiply(amount)));
        else
          newLower = new HexLocation(lower.add(direction.getDirection().multiply(amount)));
       
        if (!area.isEmpty()) {
            if (newUpper != null)
                this.area.addAll(calculateArea(upper, newUpper));
            else if (newLower != null)
                this.area.addAll(calculateArea(lower, newLower));
        }
        
        return this;
    }
    
    /**
     * Increases the size of the region by a provided amount in the direction
     * provided.
     *
     * @param direction the direction in which to expand
     * @param amount the amount of blocks to expand outwards to
     * @return a chain-able copy of the current instance
     */
    public Region contract(BlockFace direction, int amount) {
        HexLocation newUpper = null;
        HexLocation newLower = null;
    
        if (direction.getModX() + direction.getModY() + direction.getModZ() > 0)
            newLower = new HexLocation(lower.add(direction.getDirection().multiply(amount)));
        else
            newUpper = new HexLocation(upper.add(direction.getDirection().multiply(amount)));
        
        if (!area.isEmpty()) {
            if (newUpper != null)
                this.area.addAll(calculateArea(upper, newUpper));
            else if (newLower != null)
                this.area.addAll(calculateArea(lower, newLower));
        }
        
        return this;
    }
    
    /**
     * Copy the region.
     *
     * @return the copied region
     */
    public Region copy() {
        return new RectangularRegion(lower, upper);
    }
}
