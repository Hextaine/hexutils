package com.hextaine.hexutils.area;
/*
 * Created by Hextaine on 8/18/2019.
 * All questions can be directed to Hextaine@gmail.com
 */

import org.bukkit.block.BlockFace;

public interface Expandable {
    Region expand(BlockFace direction, int amount);
    
    Region contract(BlockFace direction, int amount);
}
