package com.hextaine.hexutils;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.logging.Logger;

public class HexUtils extends JavaPlugin {
    private static HexUtils hexUtils;
    private static Logger log = Logger.getLogger("Minecraft");
    private ArrayList<Listener> listeners = new ArrayList<>();
    
    @Override
    public void onEnable() {
        hexUtils = this;
    }

    @Override
    public void onDisable() {
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        return false;
    }
    
    public static HexUtils getInstance() {
        return hexUtils;
    }
    
    public boolean addListener(Listener l) {
        boolean contains = listeners.contains(l);
        if (!contains) {
            listeners.add(l);
            this.getServer().getPluginManager().registerEvents(l, this);
        }
        
        return !contains;
    }
}
