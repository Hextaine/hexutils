package com.hextaine.hexutils.inventory;
/*
 * Created by Hextaine on 11/27/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.HexUtils;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class HexInventory {
    private Inventory inv;
    
    protected HexInventory() {
        this(9, null, "");
    }
    
    public HexInventory(int size, String name) {
        this(size, null, name);
    }
    
    public HexInventory(int size, InventoryHolder holder, String name) {
        this.inv = HexUtils.getInstance().getServer().createInventory(holder, size, name);
    }
    
    public void insertItem(int slot, ActionItem item) {
        item.enable();
        this.insertItem(slot, (ItemStack) item);
    }
    
    public void insertItem(int slot, ItemStack item) {
        this.inv.setItem(slot, item);
    }
    
    public void removeItem(int slot) {
        this.inv.setItem(slot, null);
    }
    
    public void removeItem(ActionItem item) {
        this.inv.remove(item);
    }
    
    public boolean disableItem(int slot) {
        if (this.inv.getItem(slot) instanceof ActionItem) {
            return ((ActionItem)this.inv.getItem(slot)).disable();
        }
        return false;
    }
    
    public boolean enableItem(int slot) {
        if (this.inv.getItem(slot) instanceof ActionItem) {
            return ((ActionItem)this.inv.getItem(slot)).enable();
        }
        return false;
    }
    
    public void enableAll() {
        for (ItemStack i : this.inv.getContents()) {
            if (i instanceof ActionItem) {
                ((ActionItem)i).enable();
            }
        }
    }
    
    public void disableAll() {
        for (ItemStack i : this.inv.getContents()) {
            if (i instanceof ActionItem) {
                ((ActionItem)i).enable();
            }
        }
    }
    
    public ActionItem[] getActionItems() {
        ArrayList<ActionItem> actionItems = new ArrayList<>();
        for (ItemStack i : inv.getContents()) {
            if (i instanceof ActionItem)
                actionItems.add((ActionItem)i);
        }
        
        return actionItems.toArray(new ActionItem[0]);
    }
}
