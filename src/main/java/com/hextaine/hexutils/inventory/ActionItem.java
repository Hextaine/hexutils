package com.hextaine.hexutils.inventory;
/*
 * Created by Hextaine on 11/25/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.Hexecutor;
import com.hextaine.hexutils.listener.HexListener;
import com.hextaine.hexutils.listener.InventoryListener;
import org.bukkit.event.inventory.InventoryEvent;
import org.bukkit.inventory.ItemStack;

public class ActionItem extends ItemStack {
    private Hexecutor<InventoryEvent> action;
    private HexListener listener;
    
    public ActionItem(ItemStack item, Hexecutor<InventoryEvent> action) {
        super(item);
        this.action = action;
    }
    
    public boolean enable() {
        if (this.listener == null) {
            listener = new InventoryListener(this.action);
            return listener.enable();
        }
        return false;
    }
    
    public boolean disable() {
        return listener.disable();
    }
}
