package com.hextaine.hexutils.location;
/*
 * Created by Hextaine on 8/15/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * Improve the base com.hextaine.hexutils.location class by adding more complex methods or convenience methods.
 */
public class HexLocation extends Location {
    private HexLocation(World world, double x, double y, double z) {
        super(world, x, y, z);
    }

    /**
     * Base Constructor to set up locations
     * @param loc the base com.hextaine.hexutils.location
     */
    public HexLocation(Location loc) {
        super(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
    }

    /**
     * Copy constructor
     * @param loc the base com.hextaine.hexutils.location to copy off of
     */
    protected HexLocation(HexLocation loc) {
        this((Location)loc);
    }
    
    /**
     * Compares the y-value of two locations and returns whether the com.hextaine.hexutils.location the
     * operation is attached to has the higher y-value.
     *
     * @param location another com.hextaine.hexutils.location
     *
     * @return <code>true</code> if the com.hextaine.hexutils.location provided as a parameter is lower or
     *         <code>false</code> otherwise
     */
    public boolean isAbove(Location location) {
        return location.getY() < this.getY();
    }
    
    /**
     * Extracts the numerically largest X, Y, and Z from between the two locations.
     *
     * @param loc another com.hextaine.hexutils.location
     *
     * @return a new com.hextaine.hexutils.location with the numerically largest X, Y, and Z between the
     *         two locations (the one the operation is attached to and the parameter)
     */
    public HexLocation combineLowest(Location loc) {
        HexLocation lowest = new HexLocation(this);

        if (lowest.getX() > loc.getX()) {
            lowest.setX(loc.getX());
        }
        if (lowest.getY() > loc.getY()) {
            lowest.setY(loc.getY());
        }
        if (lowest.getZ() > loc.getZ()) {
            lowest.setZ(loc.getZ());
        }

        return lowest;
    }
    
    /**
     * Extracts the numerically smallest X, Y, and Z from between the two locations.
     *
     * @param loc another com.hextaine.hexutils.location
     *
     * @return a new com.hextaine.hexutils.location with the numerically smallest X, Y, and Z between the
     *         two locations (the one the operation is attached to and the parameter)
     */
    public HexLocation combineHigher(Location loc) {
        HexLocation highest = new HexLocation(this);

        if (highest.getX() < loc.getX()) {
            highest.setX(loc.getX());
        }
        if (highest.getY() < loc.getY()) {
            highest.setY(loc.getY());
        }
        if (highest.getZ() < loc.getZ()) {
            highest.setZ(loc.getZ());
        }

        return highest;
    }
    
    /**
     * Stringifies the com.hextaine.hexutils.location into it's simplist form which will still yield the
     * same block com.hextaine.hexutils.location when parsed.
     *
     * @return the int X, Y, and Z of the com.hextaine.hexutils.location as a comma-separated string
     *         terminated with a semicolon
     */
    public String toSimpleString() {
        return this.getWorld().getName() + "," + this.getBlockX() + "," + this.getBlockY() + "," + this.getBlockZ() + ";";
    }
    
    /**
     * Stringifies the com.hextaine.hexutils.location by using the exact com.hextaine.hexutils.location down to the decimal,
     * pitch, and yaw.
     *
     * @return the decimal X, Y, Z, pitch, and yaw of the com.hextaine.hexutils.location as a common-
     *         separated string terminated with a semicolon
     */
    public String toString() {
        return this.getWorld().getName() + "," + this.getX() + "," + this.getY() + "," + this.getZ() + "," + this.getPitch() + "," + this.getYaw() + ";";
    }
    
    /**
     * Parses a simple string formatted by the <code>toSimpleString</code> method
     * back into a com.hextaine.hexutils.location
     *
     * @param raw the raw simple com.hextaine.hexutils.location string containing comma-separated world
     *            name, integer x, y, and z values terminated by a semicolon
     *
     * @return a HexLocation with the integer coordinates of X, Y, and Z for the
     *         com.hextaine.hexutils.location with the default com.hextaine.hexutils.location's pitch and yaw
     */
    public static HexLocation fromSimpleString(String raw) {
        raw = raw.split(";")[0];
        
        String[] commaSeparated = raw.split(",");
        
        return new HexLocation(new Location(Bukkit.getWorld(commaSeparated[0]), Integer.parseInt(commaSeparated[1]), Integer.parseInt(commaSeparated[2]), Integer.parseInt(commaSeparated[3])));
    }

    public boolean blockEquals(HexLocation l2) {
        return (this.getBlockX() == l2.getBlockX() && this.getBlockY() == l2.getBlockY() && this.getBlockZ() == l2.getBlockZ());
    }

    public boolean equals(HexLocation l2) {
        return (this.getX() == l2.getX() && this.getY() == l2.getY() && this.getZ() == l2.getZ()
                && this.getPitch() == l2.getPitch() && this.getYaw() == l2.getYaw());
    }
}
