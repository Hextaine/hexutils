package com.hextaine.hexutils.entity;

/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.location.HexLocation;
import com.hextaine.hexutils.movement.MovementCause;
import com.hextaine.hexutils.movement.MovementTarget;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public abstract class GameEntity {
    public enum Type {
        ANIMAL, MONSTER, PLAYER, NONLIVINGENTITY
    }

    private Type type;
    protected GameEntity(Type type) {
        this.type = type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public abstract boolean teleport(MovementTarget target, MovementCause cause);

    public abstract HexLocation getLocation();
    
    public abstract void setVelocity(MovementTarget target, double strength, double addheight);
    
    public abstract void setVelocity(Vector v);
    
    public abstract Entity getEntity();
}
