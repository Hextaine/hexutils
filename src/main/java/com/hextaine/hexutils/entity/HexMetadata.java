package com.hextaine.hexutils.entity;
/*
 * Created by Hextaine on 11/28/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.HexUtils;

import java.util.HashMap;
import java.util.UUID;

public class HexMetadata {
    private static HashMap<UUID, HashMap<String, Object>> metaMap = new HashMap<>();
    
    public static void setMeta(GameEntity entity, String meta) {
        setMeta(entity, meta, null);
    }
    
    public static void setMeta(GameEntity entity, String meta, long duration) {
        setMeta(entity, meta, null);
        HexUtils.getInstance().getServer().getScheduler().runTaskLater(HexUtils.getInstance(), () -> removeMeta(entity, meta), duration);
    }
    
    public static void setMeta(GameEntity entity, String meta, int uses) {
        setMeta(entity, meta, new MetaUses(uses));
    }
    
    public static void setMeta(GameEntity entity, String meta, Object value) {
        if (hasMeta(entity, meta)) {
            removeMeta(entity, meta);
        }
        
        metaMap.get(entity.getEntity().getUniqueId()).put(meta, value);
    }
    
    public static boolean hasMeta(GameEntity entity, String meta) {
        if (!metaMap.containsKey(entity.getEntity().getUniqueId()))
            metaMap.put(entity.getEntity().getUniqueId(), new HashMap<>());
        return metaMap.get(entity.getEntity().getUniqueId()).containsKey(meta);
    }
    
    public static Object getMeta(GameEntity entity, String meta) {
        if (!hasMeta(entity, meta))
            return null;
        
        return metaMap.get(entity.getEntity().getUniqueId()).get(meta);
    }
    
    public static void removeMeta(GameEntity entity, String meta) {
        if (!hasMeta(entity, meta))
            return;
        metaMap.get(entity.getEntity().getUniqueId()).remove(meta);
    }
}

class MetaUses {
    private int uses;
    public MetaUses(int uses) {
        this.uses = uses;
    }
    
    int getUses() {
        return uses;
    }
    
    void use() {
        uses -= 1;
    }
    
    void setUses(int uses) {
        this.uses = uses;
    }
}