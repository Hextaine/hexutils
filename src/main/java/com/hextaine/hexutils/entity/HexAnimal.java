package com.hextaine.hexutils.entity;
/*
 * Created by Hextaine on 11/28/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.location.HexLocation;
import com.hextaine.hexutils.movement.MovementCause;
import com.hextaine.hexutils.movement.MovementTarget;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class HexAnimal extends GameEntity {
    private Animals animal;
    private HexLocation preTelocation;
    private HexLocation postTelocation;
    private MovementCause teleportCause;
    
    public HexAnimal(Animals animal) {
        super(Type.ANIMAL);
        this.animal = animal;
    }
    
    @Override
    public Entity getEntity() {
        return animal;
    }
    
    @Override
    public boolean teleport(MovementTarget target, MovementCause cause) {
        preTelocation = getLocation();
        postTelocation = target.getLocation();
    
        this.animal.teleport(target.getLocation());
        this.teleportCause = cause;
    
        return !getLocation().equals(postTelocation);
    }
    
    @Override
    public HexLocation getLocation() {
        return new HexLocation(animal.getLocation());
    }
    
    @Override
    public void setVelocity(MovementTarget target, double strength, double addheight) {
        Vector direct = target.getLocation().toVector();
        direct = direct.subtract(this.getLocation().toVector());
        direct = direct.add(new Vector(0, addheight, 0));
    
        animal.setVelocity(direct);
    }
    
    public void setVelocity(Vector v) {
        animal.setVelocity(v);
    }
}
