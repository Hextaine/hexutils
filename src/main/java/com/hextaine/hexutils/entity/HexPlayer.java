package com.hextaine.hexutils.entity;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.location.HexLocation;
import com.hextaine.hexutils.movement.MovementCause;
import com.hextaine.hexutils.movement.MovementTarget;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class HexPlayer extends GameEntity {
    private Player player;
    private HexLocation preTeleportLocation;
    private HexLocation postTeleportLocation;
    private MovementCause teleportCause;

    public HexPlayer(Player p) {
        super(Type.PLAYER);
        this.player = p;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    @Override
    public Entity getEntity() {
        return player;
    }

    @Override
    public boolean teleport(MovementTarget target, MovementCause cause) {
        preTeleportLocation = getLocation();
        postTeleportLocation = target.getLocation();

        this.player.teleport(target.getLocation());
        this.teleportCause = cause;
    
        return !getLocation().equals(postTeleportLocation);
    }

    @Override
    public HexLocation getLocation() {
        return new HexLocation(player.getLocation());
    }
    
    @Override
    public void setVelocity(MovementTarget target, double strength, double addheight) {
        Vector direct = target.getLocation().toVector();
        direct = direct.subtract(this.getLocation().toVector());
        direct = direct.add(new Vector(0, addheight, 0));
        
        player.setVelocity(direct);
    }
    
    public void setVelocity(Vector v) {
        player.setVelocity(v);
    }
}
