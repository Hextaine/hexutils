package com.hextaine.hexutils;
/*
 * Created by Hextaine on 11/28/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

public class HexConstants {
    public static final String REMOVE_MOTION_EFFECTS = "remove_motion";
    public static final String REMOVE_GRAVITY = "remove_gravity";
}
