package com.hextaine.hexutils.listener;
/*
 * Created by Hextaine on 11/24/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.Hexecutor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ItemInteractionListener extends HexListener {
    private ItemStack item;
    private Hexecutor<PlayerInteractEvent> exec;
    
    public ItemInteractionListener(ItemStack i, Hexecutor<PlayerInteractEvent> exec) {
        this.item = i;
        this.exec = exec;
    }
    
    @EventHandler
    public void onClick(PlayerInteractEvent e) {
        if (isEnabled())
            exec.execute(e);
    }
}
