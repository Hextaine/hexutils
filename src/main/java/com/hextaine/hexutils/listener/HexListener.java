package com.hextaine.hexutils.listener;
/*
 * Created by Hextaine on 11/24/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.event.Listener;

public abstract class HexListener implements Listener {
    private boolean enabled;
    
    public HexListener() {
        this.enabled = false;
    }
    
    public boolean enable() {
        return setEnabled(true);
    }
    public boolean disable() {
        return setEnabled(false);
    }
    public boolean isEnabled() {
        return this.enabled;
    }
    protected boolean setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this.enabled;
    }
}
