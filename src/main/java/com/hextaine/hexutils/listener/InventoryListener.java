package com.hextaine.hexutils.listener;
/*
 * Created by Hextaine on 11/25/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.Hexecutor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryEvent;

public class InventoryListener extends HexListener {
    private Hexecutor<InventoryEvent> action;
    
    public InventoryListener(Hexecutor<InventoryEvent> action) {
        this.action = action;
    }
    
    @EventHandler
    public void handleInventoryClick(InventoryEvent event) {
        if (isEnabled())
            action.execute(event);
    }
}
