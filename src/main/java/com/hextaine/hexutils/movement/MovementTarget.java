package com.hextaine.hexutils.movement;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.location.HexLocation;

public interface MovementTarget {
//    PLAYER, ANIMAL, MONSTER, BLOCK, LOCATION, HEXENTITY

    HexLocation getLocation();
}
