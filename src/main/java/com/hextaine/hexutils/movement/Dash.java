package com.hextaine.hexutils.movement;
/*
 * Created by Hextaine on 11/27/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.entity.GameEntity;
import com.hextaine.hexutils.location.HexLocation;

public class Dash {
    private GameEntity entity;
    private MovementTarget target;
    private double strength;
    private double addHeight;
    private HexLocation start;
    private boolean completed;
    private double gravity;
    private GravityManipulator gravityManip;
    
    public Dash(GameEntity entity, MovementTarget target, double strength) {
        this(entity, target, strength,1);
    }
    
    public Dash(GameEntity entity, MovementTarget target, double strength, double addHeight) {
        this(entity, target, strength, addHeight, 1);
    }
    
    public Dash(GameEntity entity, MovementTarget target, double strength, double addHeight, double gravity) {
        this.entity = entity;
        this.target = target;
        this.strength = strength;
        this.addHeight = addHeight;
        this.gravity = gravity;
        this.start = entity.getLocation();
    }
    
    public void dash() {
        start = entity.getLocation();
        if (!completed) {
            entity.setVelocity(target, strength, addHeight);
            
            if (gravity != 1)
                gravityManip = new GravityManipulator(entity, gravity);
            
            setCompleted(true);
        }
    }
    
    public boolean isCompleted() {
        return completed;
    }
    
    public void setCompleted(boolean c) {
        this.completed = c;
    }
}
