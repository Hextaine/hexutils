package com.hextaine.hexutils.movement;/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.entity.GameEntity;

public class Teleport {
    public enum Type {
        LOCATION, ANIMAL, MONSTER, PLAYER, NONLIVINGENTITY
    }

    private Type teleportType;
    private GameEntity entity;
    private MovementTarget target;
    private MovementCause cause;
    private boolean completed;

    private Teleport() {}
    public Teleport(Type type, GameEntity entity, MovementTarget target, MovementCause cause) {
        this.teleportType = type;
        this.entity = entity;
        this.target = target;
        this.cause = cause;
        completed = false;
    }

    public void teleport() {
        if (completed)
            return;
        
        entity.teleport(target, cause);
    }

    public Type getTeleportType() {
        return teleportType;
    }

    public void setTeleportType(Type teleportType) {
        this.teleportType = teleportType;
    }

    public GameEntity getEntity() {
        return entity;
    }

    public void setEntity(GameEntity entity) {
        this.entity = entity;
    }

    public MovementTarget getTarget() {
        return target;
    }

    public void setTarget(MovementTarget target) {
        this.target = target;
    }

    public boolean isComplete() {
        return completed;
    }
}
