package com.hextaine.hexutils.movement;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.location.HexLocation;
import org.bukkit.entity.Player;

public class PlayerTarget implements MovementTarget {
    private Player player;

    public PlayerTarget(Player p) {
        this.player = p;
    }

    @Override
    public HexLocation getLocation() {
        return new HexLocation(player.getLocation());
    }
}
