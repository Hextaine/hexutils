package com.hextaine.hexutils.movement;
/*
 * Created by Hextaine on 11/28/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.hexutils.HexConstants;
import com.hextaine.hexutils.HexUtils;
import com.hextaine.hexutils.entity.GameEntity;
import com.hextaine.hexutils.entity.HexMetadata;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.UUID;

public class GravityManipulator implements Runnable {
    private static HashMap<UUID, GravityManipulator> activeGravity = new HashMap<>();
    
    private BukkitTask task;
    private GameEntity entity;
    private double gravityMult;
    private double lastVertVelocity;
    
    public GravityManipulator(GameEntity entity, double gravityMult) {
        this.entity = entity;
        this.gravityMult = gravityMult;
        lastVertVelocity = 0;
    }
    
    public BukkitTask enable(long duration, boolean endOnGround) {
        if (task != null)
            this.disable();
        
        this.enable(endOnGround);
        
        final BukkitTask toCancel = task;
        HexUtils.getInstance().getServer().getScheduler().runTaskLater(HexUtils.getInstance(), toCancel::cancel, duration);
        
        return this.task;
    }
    
    public BukkitTask enable(boolean endOnGround) {
        if (task != null)
            this.disable();
        
        this.task = HexUtils.getInstance().getServer().getScheduler().runTaskTimer(HexUtils.getInstance(), this, 0, 1);
        
        if (this.gravityMult != 1) {
            if (activeGravity.containsKey(entity.getEntity().getUniqueId())) {
                activeGravity.get(entity.getEntity().getUniqueId()).disable();
                activeGravity.put(entity.getEntity().getUniqueId(), this);
            }
        }
        
        return this.task;
    }
    
    public void disable() {
        if (task != null) {
            this.task.cancel();
            
            activeGravity.remove(entity.getEntity().getUniqueId());
        }
    }
    
    @Override
    public void run() {
        if (entity.getEntity().getVelocity().getY() <= 1)
            return;
        
        if (Math.abs(lastVertVelocity) < 0.1 && Math.abs(entity.getEntity().getVelocity().getY()) < 0.1)
            return;
        if (lastVertVelocity < 0 && entity.getEntity().getVelocity().getY() > 0)
            return;
        if (HexMetadata.hasMeta(entity, HexConstants.REMOVE_MOTION_EFFECTS) || HexMetadata.hasMeta(entity, HexConstants.REMOVE_GRAVITY)) {
            this.disable();
            return;
        }
        
        Vector velocity = entity.getEntity().getVelocity();
        velocity = velocity.multiply(new Vector(1, gravityMult, 1));
        
        entity.setVelocity(velocity);
        
        lastVertVelocity = velocity.getY();
    }
    
}
